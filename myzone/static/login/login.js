function intro_web_opacity_show(obj)
{
	$(obj).css({opacity:1});
}
function intro_web_opacity_hide(obj)
{
	$(obj).css({opacity:0.7});
}

$(document).ready(function(){
	//show intro_web
	$("#intro_web_1").fadeIn(1500);
	setTimeout(function(){
		$("#intro_web_2").fadeIn(1500);
	},1000);
	setTimeout(function(){
		$("#intro_web_3").fadeIn(1500);
	},1500);
	setTimeout(function(){
		$(".panel").fadeIn(1000);
	},2000);

	//clean email_tip and password_tip
	function reset_modal()
	{
		$("#email_tip span").remove();
		$("#password_tip span").remove();
	}

	$("#login_button").click(function(){
			reset_modal();
	});

	//login_button show
	$("#sign_login").mouseenter(function(){
			$(this).css({opacity:1});
	});
	$("#sign_login").mouseleave(function(){
			$(this).css({opacity:0.8});
	});

	//login_button show
	$("#submit").mouseenter(function(){
			$(this).css({opacity:1});
	});
	$("#submit").mouseleave(function(){
			$(this).css({opacity:0.6});
	});

	//signin_button show
	$("#Signin").mouseenter(function(){
			$(this).css({opacity:1});
	});
	$("#Signin").mouseleave(function(){
			$(this).css({opacity:0.6});
	});

	//email format check
	function checkemail(email)
	{
			 var reg = /\w+@\w+\.\w+/;
			 if (email.match(reg,email))
			 {
					return true;
			 }
			 else
			 {
					 return false;
			 }
	 }

	 //name format check
	 function checkname(name)
	 {
		 var reg=/^[A-Za-z]+$/;
		 if(name.match(reg,name))
		 {
			 return true;
		 }
		 else
		 {
		 		return false;
		 }
	 }

	 //phonenumber format check
	 function checkphonenumber(phonenumber)
	 {
		  var reg=/^\d+$/;
			if(phonenumber.match(reg,phonenumber))
			{
				return true;
			}
			else
			{
				 return false;
			}
	 }

	//login verify
	$("#submit").click(function(){
	reset_modal();
	var username=$("#username").val();
	var password=$("#password").val();
	if(username=="")
	{
		 var check_exist=$("#email_tip span").text();
		 if(check_exist!="Email cannot be empty")
		 {
			 $("#email_tip").append("<span style=\"color:orange;\"> Email cannot be empty</span>");
		 }
		 //$("#password_tip").append("<span style=\"color:red;\">password cannot be empty</span>")
	}
	if(password=="")
	{
		var check_exist=$("#password_tip span").text();
		if(check_exist!="Password cannot be empty")
		{
			$("#password_tip").append("<span style=\"color:orange;\"> Password cannot be empty</span>");
		}
	}
	if(username!="" && password!="")
	{
			$.ajax({
				type:"POST",
				url:"index.php/user/verify",
				data:{"username":username,"password":password},
				dataType:"json",
				async:true,
				success:function(message)
				{
					if(message=="success")
					{
						window.location.href="index.php/user/homepage";
					}
					else
					{
						reset_modal();
						$("#email_tip").append("<span style=\"color:red;\"> Incorrect Email or Password</span>");
					}
				},
				error:function()
				{
					alert("ajax false");
				}
		});
	}

	});



	//clean all signin tip
	function reset_signin_modal()
	{
		$("#signin_email_tip span").remove();
		$("#signin_password_tip span").remove();
		$("#signin_lastname_tip span").remove();
		$("#signin_firstname_tip span").remove();
		$("#signin_phonenumber_tip span").remove();
	}
	function reset_allsignin_panel()
	{
		reset_signin_modal();
		$("#signin_email").val("");
		$("#signin_password").val("");
		$("#signin_lastname").val("");
		$("#signin_firstname").val("");
		$("#signin_phonenumber").val("");
	}

	//signin verify
	$("#Signin").click(function(){
	reset_signin_modal();
	var email=$("#signin_email").val();
	var password=$("#signin_password").val();
	var lastname=$("#signin_lastname").val();
	var firstname=$("#signin_firstname").val();
	var phonenumber=$("#signin_phonenumber").val();

	//email format check
	var check_email=checkemail(email);
	var lastname_check=checkname(lastname);
	var firstname_check=checkname(firstname);
	var phonenumber_check=checkphonenumber(phonenumber);

	if(email!="" && check_email==false)
	{
			$("#signin_email_tip").append("<span style=\"color:red;\"> Email format is invalid</span>");
	}
	if(email=="")
	{
			 $("#signin_email_tip").append("<span style=\"color:orange;\"> Email is required</span>");
	}
	if(password=="")
	{
			$("#signin_password_tip").append("<span style=\"color:orange;\"> Password is required</span>");
	}
	if(lastname=="")
	{
			$("#signin_lastname_tip").append("<span style=\"color:orange;\"> Lastname is required</span>");
	}
	if(lastname!="" && lastname_check==false)
	{
			$("#signin_lastname_tip").append("<span style=\"color:red;\">  Only letter is required.</span>");
	}
	if(firstname=="")
	{
			$("#signin_firstname_tip").append("<span style=\"color:orange;\"> Firstname is required</span>");
	}
	if(firstname!="" && firstname_check==false)
	{
			$("#signin_firstname_tip").append("<span style=\"color:red;\"> Only letter is required</span>");
	}
	if(email!="" && password!="" && lastname!="" && firstname!="" && check_email==true)
	{
			$.ajax({
				type:"POST",
				url:"index.php/user/signin",
				data:{"email":email,"password":password,"lastname":lastname,"firstname":firstname,"phonenumber":phonenumber},
				async:true,
				dataType:"json",
				success:function(message)
				{
					if(message==false)
					{
						reset_signin_modal();
						$("#signin_email_tip").append("<span style=\"color:red;\"> Email already exists!</span>");
					}
					else
					{
						reset_allsignin_panel();
						$("#signin_area .panel").hide();
						$("#signin_area .sign_up_success").fadeIn();
					}
				},
				error:function()
				{
					alert("ajax false");
				}
		});

	}

	});




});
