//dropdown meanu color
function change_color(obj)
{
    $(obj).css({background:"#FFFFFF"});
}
function reset_color(obj)
{
    $(obj).css({background:"rgb(236,236,236)"});
}

$(document).ready(function(){

      //tips
      $("#start").hide();
      $("#speedcontrol").hide();
      $("#pause").hide();
      $("#restart").hide();
      $(".panel-body").append("<p>Version 1 has border.Version 2 without border</p>");
      $(".panel-body").append("<table class=\"table table-bordered\"></table>");

      //make table
      var i=0;
      var j=0;
      for(i=0;i<24;i++)
      {
        $("table").append("<tr id=\"tr_"+ i +"\"></tr>");
        for(j=0;j<45;j++)
        {
          $('#tr_'+ i).append("<td id=\"tr_"+ i +"_td_" + j + "\"></td>");
        }
      }
      $("#tr_15_td_3").css({background:"red"});


      //food class
      var food=new Object();
      food.tr=0;
      food.td=0;
      food.flag=0;
      food.random_food_tr=function(){
        var number=Math.floor(Math.random()*(24));
        return number;
      };
      food.random_food_td=function(){
        var number=Math.floor(Math.random()*(45));
        return number;
      };
      food.creat_food=function(){
        food.tr=food.random_food_tr();
        food.td=food.random_food_td();
        for(var i=0;i<snake_array.length;i++)
        {
          while(food.tr==snake_array[i].tr && food.td==snake_array[i].td)
          {
            food.tr=food.random_food_tr();
            food.td=food.random_food_td();
          }
        }
        $("#tr_"+food.tr+"_td_"+food.td).css({background:"yellow"});
      };


      //snake class
      var snake=new Object();
      snake.tr=15;
      snake.td=3;
      snake.position="";
      snake.time=null;
      snake.speed=120;
      snake.pretr=15;
      snake.pretd=3;
      snake.version=1;

      //snake array
      var snake_array=new Array();
      snake_array.push(snake);

      //reset
      snake.reset=function(){
        clearInterval(snake.time);
        $(".panel-body").append("<p>Game over！Please click restart！</p>");
        $("#pause").hide();

        for(var i=0;i<snake_array.length;i++)
        {
          $("#tr_"+snake_array[i].tr+"_td_"+snake_array[i].td).css({background:"white"});
        }
        for(var i=0;i<snake_array.length-1;i++)
        {
          snake_array.splice(1,1);
        }
        snake_array.length=1;
        $("#tr_"+food.tr+"_td_"+food.td).css({background:"white"});
        snake.tr=15;
        snake.td=3;
        snake.position="";
        snake.time=null;
        snake.version=1;
        $("#tr_15_td_3").css({background:"red"});
        food.tr=0;
        food.td=0;
        food.flag=0;

      };

      snake.show=function(direction){
        $("#tr_"+snake.tr+"_td_"+snake.td).css({background:"red"});
        if(snake_array.length==1)
        {
          if(direction==1)
          {
            $("#tr_"+snake.tr+"_td_"+snake.pretd).css({background:"white"});
          }
          else
          {
            $("#tr_"+snake.pretr+"_td_"+snake.td).css({background:"white"});
          }
        }
      }



      //move function
      snake.move=function()
      {
            //initial a food
            if(food.flag==0)
            {
              food.creat_food();
              food.flag=1;
            }
            snake.time=setInterval(function(){

            var position=snake.position;

            snake.pretd=snake.td;
            snake.pretr=snake.tr;

            if(position=="right")
            {
              if(snake.td == 44)
              {
                if(snake.version==2)
                {
                  snake.reset();
                    return;
                }
                snake.td=0;
              }
              else
              {
                snake.td=snake.td+1;
              }
                snake.show(1);

            }
            else if (position=="left")
            {
              if(snake.td == 0)
              {
                if(snake.version==2)
                {
                  snake.reset();
                    return;
                }
                snake.td=44;
              }
              else
              {
                snake.td=snake.td-1;
              }
                snake.show(1);
            }
            else if (position=="up")
            {
              if(snake.tr == 0)
              {
                if(snake.version==2)
                {
                  snake.reset();
                    return;
                }
                snake.tr=23;
              }
              else
              {
                snake.tr=snake.tr-1;
              }
                snake.show(2);
            }
            else if (position=="down")
            {
              if(snake.tr == 23)
              {
                if(snake.version==2)
                {
                  snake.reset();
                    return;
                }
                snake.tr=0;
              }
              else
              {
                snake.tr=snake.tr+1;
              }
                snake.show(2);

            }

            for(var j=1;j<snake_array.length;j++)
            {

                snake_array[j].tr=snake_array[j-1].pretr;
                snake_array[j].td=snake_array[j-1].pretd;
            }


            //eat food
            if(snake.tr==food.tr && snake.td==food.td)
            {
              var new_snake=Object();
              var length=snake_array.length-1;
              var tr_num=(snake_array[length].tr)-(snake_array[length].pretr);
              var td_num=(snake_array[length].td)-(snake_array[length].pretd);
              //right
              if(tr_num==0 && td_num>0)
              {
                new_snake.tr=snake_array[length].tr;
                new_snake.td=snake_array[length].td-1;
              }
              //left
              else if (tr_num==0 && td_num<0)
              {
                new_snake.tr=snake_array[length].tr;
                new_snake.td=snake_array[length].td+1;
              }
              //up
              else if (tr_num<0 && td_num==0)
              {
                new_snake.tr=snake_array[length].tr+1;
                new_snake.td=snake_array[length].td;
              }
              //down
              else if (tr_num>0 && td_num==0)
              {
                new_snake.tr=snake_array[length].tr-1;
                new_snake.td=snake_array[length].td;
              }
              new_snake.pretr=new_snake.tr;
              new_snake.pretd=new_snake.td;

                  $("#tr_"+new_snake.tr+"_td_"+new_snake.td).css({background:"red"});

                  new_snake.tr=snake_array[length].pretr;
                  new_snake.td=snake_array[length].pretd;

                  snake_array.push(new_snake);
              food.creat_food();
            }

            for(var j=1;j<snake_array.length;j++)
            {
                $("#tr_"+snake_array[j].pretr+"_td_"+snake_array[j].pretd).css({background:"white"});
                $("#tr_"+snake_array[j].tr+"_td_"+snake_array[j].td).css({background:snake.bodycolor});
                snake_array[j].pretr=snake_array[j].tr;
                snake_array[j].pretd=snake_array[j].td;
            }

            for(j=1;j<snake_array.length;j++)
            {
              if(snake.tr==snake_array[j].tr && snake.td==snake_array[j].td)
                {
                  $("#tr_"+snake_array[j].tr+"_td_"+snake_array[j].td).css({background:"red"});
                  snake.reset();
                  return;
                }
            }

          },snake.speed);

      }

      snake.pause=function(){
         clearInterval(snake.time);
         $(".panel-body").append("<p>Game pause，Please click start game to continue</p>");
         $("#start").show();
         $("#pause").hide();
         $("#speedcontrol").show();
      };
      snake.restart=function(){
        snake.reset();
        $(".panel-body p").remove();
        $("#restart").hide();


        $(".panel-body").append("<p>Version 1 has border.Version 2 without border</p>");
        $("#game1").show();
        $("#game2").show();
      }

      snake.setSpeed=function(){
        snake.speed=$("#speedcontrol").val();
        snake.speed=parseInt(snake.speed);
      };

      snake.bodycolor=function()
      {
          var color='#'+Math.floor(Math.random()*16777215).toString(16);
            return color;
      }



      //main function
      //click to select version
      $("#game2").click(function(){
        $(".panel-body p").remove();
        snake.version=2;
        $("#game1").hide();
        $("#game2").hide();
        $("#start").show();
        $("#speedcontrol").show();
        $(".panel-body").append("<p>After click start game,press W,S,A,D to move</p>");

      });
      $("#game1").click(function(){
        $(".panel-body p").remove();
        snake.version=1;
        $("#game1").hide();
        $("#game2").hide();
        $("#start").show();
        $("#speedcontrol").show();
        $(".panel-body").append("<p>After click start game,press W,S,A,D to move</p>");
      });

      //start click
      $("#start").click(function(){

        $("#game1").hide();
        $("#game2").hide();
        $("#speedcontrol").hide();
        $("#pause").show();
        $("#restart").show();
        $(".panel-body p").remove();
        $("#start").hide();

        //keyboard control
        $(window).keydown(function(event)
        {
            var input= event.keyCode;
          if(input=="68" && snake.position!="left")
          {
            snake.position="right";
          }
          else if (input=="87" && snake.position!="down")
          {
            snake.position="up";
          }
          else if (input=="83" && snake.position!="up")
          {
            snake.position="down";
          }
          else if (input=="65" && snake.position!="right")
          {
            snake.position="left";
          }
        });
        //start move
        snake.setSpeed();

        snake.move();

      });

      //pasue event
      $("#pause").click(function(){
        snake.pause();
      });

      //restart event
      $("#restart").click(function()
      {
        snake.restart();

      });



});
