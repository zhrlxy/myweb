//dropdown meanu color
function change_color(obj)
{
		$(obj).css({background:"#FFFFFF"});
}
function reset_color(obj)
{
		$(obj).css({background:"rgb(236,236,236)"});
}

$(document).ready(function(){
	$("h1").fadeIn(1000,function(){
		$("#t1").fadeIn(1000);
	});
});

function go()
{
	$("#t1 tbody tr").each(function(){
		$(this).children("td").each(function(){
			alert($(this).text());
		});
	});
}

function skill(obj)
{
	var skillname=$(obj).text();
	var skillnum="0";
	var skillwei="0";
	var skillpic="pic";
	if(skillname == "影杀卷：")
	{
		skillpic="/picture/gamecalculator/影杀.gif";
		skillnum="1611~2685";
		skillwei="200%";
	}
	else if(skillname == "狂影剑：")
	{
		skillpic="/picture/gamecalculator/狂影.gif";
		skillnum="1289~2148";
		skillwei="160%";
	}
	else if(skillname == "残影剑：")
	{
		skillpic="/picture/gamecalculator/残影.gif";
		skillnum="896~1494";
		skillwei="55%";
	}
	else if(skillname == "疾影剑：")
	{
		skillpic="/picture/gamecalculator/疾影.gif";
		skillnum="1550~2584";
		skillwei="55%";
	}
	else if(skillname == "断魂剑：")
	{
		skillpic="/picture/gamecalculator/断魂.gif";
		skillnum="969~1615";
		skillwei="55%";
	}
	else if(skillname == "饮血剑：")
	{
		skillpic="/picture/gamecalculator/饮血.gif";
		skillnum="1058~1763";
		skillwei="50%";
	}
	else if(skillname == "回环剑：")
	{
		skillpic="/picture/gamecalculator/回环.gif";
		skillnum="158~264";
		skillwei="28%";
	}
	else if(skillname == "暗器卷：")
	{
		skillpic="/picture/gamecalculator/暗器.gif";
		skillnum="302~504";
		skillwei="30%";
	}
	else
	{
		skillpic="/picture/gamecalculator/百影.gif";
		skillnum="1074~1790";
		skillwei="100%";
	}
	$("#t3 tr:eq(0)").append("<td><span>"+skillname+"</span><img src="+skillpic+"></td><td><span>Skill damage:</span> <span>"+skillnum+"</span></td><td><span>Skill power:</span> <span>"+skillwei+"</span></td>");
	$("#t1").fadeOut(1000,function(){
		$("#t3").fadeIn(1000,function(){
			$("#t2").fadeIn();
		});
	});
}


function check_number(number)
{
		 var reg=/^\d+$/;
		 if(number.match(reg,number))
		 {
			 return true;
		 }
		 else
		 {
				return false;
		 }
}

function claculate()
{
	var check_1=check_number($("#gongli").val());
	var check_2=check_number($("#fushang").val());
	var check_3=check_number($("#zhuxin").val());

	if($("#gongli").val()!="" && $("#fushang").val()!="" && $("#zhuxin").val()!="" && check_1==true && check_2==true && check_3==true)
	{
		$("#t2").hide();

		var snum=$("#skname td:eq(1) span:last-child").text();
		var swei=$("#skname td:eq(2) span:last-child").text();

		var arr1=snum.split('~');

		var xiaogong=parseInt(arr1[0]);
		var dagong=parseInt(arr1[1]);


		var arr2=swei.split('%');
		var weili=parseInt(arr2[0]);


		var persongongli=parseInt($("#gongli").val());
		var personfushang=parseInt($("#fushang").val());
		var personzhuxin=parseInt($("#zhuxin").val());

		weili=(weili/100).toFixed(2);
		weili=parseFloat(weili);
		personzhuxin=Math.ceil(parseFloat((personzhuxin/19.2).toFixed(2)));
		personzhuxin=(personzhuxin/100).toFixed(2);
		personzhuxin=parseFloat(personzhuxin);
		var summin=((xiaogong+persongongli+personfushang)*weili)*(1.4+personzhuxin);
		var summax=((dagong+persongongli+personfushang)*weili)*(1.6+personzhuxin);

		$("#t4 tr:eq(0)").append("<td><span>Min-Damage:</span><span>"+summin+"</span></td><td><span>Max-Damage:</span><span>"+summax+"</span></td>");
		$("#t4").fadeIn(1000);
	}

	$("#t4 button").click(function(){
		window.location.href="game_calculator";
	});


}
