var logout_flag=0;

window.onbeforeunload = function ()
{
  if(logout_flag==0)
  {
    var userAgent = navigator.userAgent.toLowerCase();
    if(userAgent.indexOf("msie")>-1)
    { //IE
        $.ajax({ url:"logout", crossDomain: true, async: false, dataType: "jsonp" });
    }
    else
    { //FireFox Chrome
        $.ajax({ url:"logout", async: false });
    }
  }

}

//dropdown meanu color
function change_color(obj)
{
    $(obj).css({background:"#FFFFFF"});
}
function reset_color(obj)
{
    $(obj).css({background:"rgb(236,236,236)"});
}

$("#message_board").fadeIn(1000,function(){
  $("#live_chat").fadeIn(1000);
});


function message_input_submit()
{
  var message=$("#message_board .panel-footer .input-group input").val();
  $("#message_board .panel-footer .input-group input").val("");
  if(message!="")
  {
    $.ajax({
      type:"POST",
      url:"send_messageboard",
      dataType:"json",
      data:{"message":message},
      async:true,
      success:function(back)
      {
        $("#message_board .panel-body table").append("<tr><td style=\"color:red;\"><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span>"+" "+back['firstname']+" "+back['lastname']+"</p><p class=\"message_time\"><span class=\"glyphicon glyphicon-time\"></span>"+" "+back['date']+"</p><p class=\"message_content\">"+back['message']+"</p></td></tr>");
        $('#message_board .panel-body').scrollTop( $('#message_board .panel-body')[0].scrollHeight );
      },
      error:function()
      {
        //alert("ajax false2");
      }

    });
  }

}


//when getinto chatzone page, update the chatboard
$.ajax({
  type:"POST",
  dataType:"json",
  async:true,
  url:"show_messageboard",
  success:function(back){
    for(var key in back)
    {
      var myid=parseInt($("#id_get").text());
      if(myid==back[key]['user_id'])
      {
        $("#message_board .panel-body table").append("<tr><td style=\"color:red;\"><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span>"+" "+back[key]['firstname']+" "+back[key]['lastname']+"</p><p class=\"message_time\"><span class=\"glyphicon glyphicon-time\"></span>"+" "+back[key]['message_time']+"</p><p class=\"message_content\">"+back[key]['message']+"</p></td></tr>");
      }
      else
      {
        $("#message_board .panel-body table").append("<tr><td><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span>"+" "+back[key]['firstname']+" "+back[key]['lastname']+"</p><p class=\"message_time\"><span class=\"glyphicon glyphicon-time\"></span>"+" "+back[key]['message_time']+"</p><p class=\"message_content\">"+back[key]['message']+"</p></td></tr>");
      }
      $('#message_board .panel-body').scrollTop( $('#message_board .panel-body')[0].scrollHeight );
    }
  },
  error:function(){
    //alert("ajax false1");
  }
});

//show all users who signup
$.ajax({
  type:"POST",
  url:"admin_show_all_chat_members",
  data:{},
  dataType:"json",
  success:function(back)
  {
    for(var key in back)
    {
      $("#live_chat .panel-body table").append("<tr id='user"+back[key]['id']+"' onclick=\"open_chat(this)\"><td><p><span class=\"glyphicon glyphicon-user\"></span>"+"<p3> "+back[key]['firstname']+" "+back[key]['lastname']+"</p3><p2>"+back[key]['id']+"</p2></p><span class=\"user_state\"><span class=\"glyphicon glyphicon-eye-close\"></span></span></td></tr>");
    }
  },
  error:function()
  {
    //alert("ajax false3");
  }
});

//open chat window,receive chat message
var receive_timer=null;
var receive_ajax_stop=null;
function open_chat(obj)
{
  $("#live_chat_window .panel-heading h3").text("Live Chat with"+$(obj).children("td").children("p").children("p3").text());
  $("#live_chat_window .panel-heading h3").append("<p style=\"display:none;\">"+$(obj).children("td").children("p").children("p2").text()+"</p>")
  $("#live_chat").fadeOut(500,function(){
    $("#live_chat_window").fadeIn(500);
  });
  //alert($(obj).children("td").children("p").children("p2").text());
  var sender_id=$(obj).children("td").children("p").children("p2").text();
  var sender_name=$(obj).children("td").children("p").children("p3").text();
  //receive live message
  receive_timer=setInterval(function(){
    receive_ajax_stop=$.ajax({
        type:"POST",
        url:"live_chat_receive_message",
        data:{"sender_id":sender_id},
        dataType:"json",
        success:function(back)
        {
          for(var key in back)
          {
            $("#live_chat_window .panel-body table").append("<tr><td><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span> "+sender_name+" <span class=\"live_time\">"+back[key]['message_time']+"</span></p><p class=\"message_content\">"+back[key]['note']+"</p></td></tr>");
            $('#live_chat_window .panel-body').scrollTop( $('#live_chat_window .panel-body')[0].scrollHeight );
            $("#user_new_music")[0].play();
          }
          $(obj).css({border:"0px solid red"});
        },
        error:function()
        {
          //alert("ajax false receive");
        }
    });
  },1000);

}


//close chat window
$("#live_chat_window .panel-heading .close").click(function(){
    clearInterval(receive_timer);
    receive_timer=null;

    $("#live_chat_window").fadeOut(500,function(){
      $("#live_chat").fadeIn(500);
    });
    $("#live_chat_window .panel-body table tr").remove();
});


//send live message
function chat_input_submit()
{
  var receiver_id=$("#live_chat_window .panel-heading").children("h3").children("p").text();
  var message=$("#live_chat_window .panel-footer input").val();
  if(message!="")
  {
    $("#live_chat_window .panel-footer input").val("");

    $.ajax({
      type:"POST",
      url:"live_chat_send_message",
      data:{"receiver_id":receiver_id,"message":message},
      dataType:"json",
      async:true,
      success:function(back)
      {
        $("#live_chat_window .panel-body table").append("<tr><td><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span> "+back['firstname']+" "+back['lastname']+" <span class=\"live_time\">"+back['message_time']+"</span></p><p class=\"message_content\">"+back['message']+"</p></td></tr>");
        $('#live_chat_window .panel-body').scrollTop( $('#live_chat_window .panel-body')[0].scrollHeight );
      },
      error:function()
      {
        //alert("ajax false4");
      }
    });
  }
}

var check_tome=null;
//check_each_user_message_state
setTimeout(function(){
  check_tome=setInterval(function(){
    $.ajax({
      type:"POST",
      url:"admin_check_user_message_tome",
      data:{},
      dataType:"json",
      success:function(back)
      {
          for(var key in back)
          {
            var select="#user"+back[key]['sender_id'];
            $(select).css({border:"1px solid red"});
          }
      },
      error:function()
      {
        //alert("ajax false check_user_message_tome");
      }
    });
  },1000);
},1000);

var check_user_state=null;

//check user online or outline
check_user_state=setInterval(function(){
  $.ajax({
    type:"POST",
    url:"check_user_online",
    data:{},
    dataType:"json",
    async:true,
    success:function(back)
    {
      for(var key in back)
      {
        var select="#user"+back[key]['id'];
        if(back[key]['oneline_state']==1)
        {
          $(select).children("td").children(".user_state").children("span").remove();
          $(select).children("td").children(".user_state").append("<span class=\"glyphicon glyphicon-eye-open\"></span>");
        }
        else
        {
          $(select).children("td").children(".user_state").children("span").remove();
          $(select).children("td").children(".user_state").append("<span class=\"glyphicon glyphicon-eye-close\"></span>");
        }
      }
    },
    error:function()
    {
    }
  });
},500);

//logout
function logout()
{
  logout_flag=1;
  clearInterval(receive_timer);
  clearInterval(check_tome);
  clearInterval(check_user_state);
  window.location.href="logout";
}
