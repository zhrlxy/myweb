var logout_flag=0;

window.onbeforeunload = function ()
{
  if(logout_flag==0)
  {
    var userAgent = navigator.userAgent.toLowerCase();
    if(userAgent.indexOf("msie")>-1)
    { //IE
        $.ajax({ url:"logout", crossDomain: true, async: false, dataType: "jsonp" });
    }
    else
    { //FireFox Chrome
        $.ajax({ url:"logout", async: false });
    }
  }
}

//dropdown meanu color
function change_color(obj)
{
    $(obj).css({background:"#FFFFFF"});
}
function reset_color(obj)
{
    $(obj).css({background:"rgb(236,236,236)"});
}

$("#message_board").fadeIn(1000,function(){
  $("#live_chat_tip").fadeIn(1000);
});

var receive_timer=null;
var receive_ajax_stop=null;
$("#live_chat_tip .panel-footer button").click(function(){
  $("#live_chat_tip").fadeOut(500,function(){
      $("#live_chat").fadeIn(500);
    });
 //receive live message
  var sender_id=18;
  var sender_name="huanrong Zhang";
  receive_timer=setInterval(function(){
    receive_ajax_stop=$.ajax({
        type:"POST",
        url:"live_chat_receive_message",
        data:{"sender_id":sender_id},
        dataType:"json",
        success:function(back)
        {
          for(var key in back)
          {
            $("#live_chat .panel-body table").append("<tr><td><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span> "+sender_name+" <span class=\"live_time\">"+back[key]['message_time']+"</span></p><p class=\"message_content\">"+back[key]['note']+"</p></td></tr>");
            $('#live_chat .panel-body').scrollTop( $('#live_chat .panel-body')[0].scrollHeight );
            $("#user_new_music")[0].play();
          }
        },
        error:function()
        {
          //alert("ajax false receive live message");
        }
    });
  },1000);
});

//send message to message_board
function message_input_submit()
{
  //get current time
  var message=$("#message_board .panel-footer .input-group input").val();
  $("#message_board .panel-footer .input-group input").val("");
  if(message!="")
  {
    $.ajax({
      type:"POST",
      url:"send_messageboard",
      dataType:"json",
      data:{"message":message},
      async:true,
      success:function(back)
      {
        $("#message_board .panel-body table").append("<tr><td style=\"color:red;\"><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span>"+" "+back['firstname']+" "+back['lastname']+"</p><p class=\"message_time\"><span class=\"glyphicon glyphicon-time\"></span>"+" "+back['date']+"</p><p class=\"message_content\">"+back['message']+"</p></td></tr>");
        $('#message_board .panel-body').scrollTop( $('#message_board .panel-body')[0].scrollHeight );
      },
      error:function()
      {
        //alert("ajax false send message to message_board");
      }

    });
  }
}

//when getinto chatzone page, update the chatboard
$.ajax({
  type:"POST",
  dataType:"json",
  async:true,
  url:"show_messageboard",
  success:function(back){
    for(var key in back)
    {
      var myid=parseInt($("#id_get").text());
      if(myid==back[key]['user_id'])
      {
        $("#message_board .panel-body table").append("<tr><td style=\"color:red;\"><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span>"+" "+back[key]['firstname']+" "+back[key]['lastname']+"</p><p class=\"message_time\"><span class=\"glyphicon glyphicon-time\"></span>"+" "+back[key]['message_time']+"</p><p class=\"message_content\">"+back[key]['message']+"</p></td></tr>");
      }
      else
      {
        $("#message_board .panel-body table").append("<tr><td><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span>"+" "+back[key]['firstname']+" "+back[key]['lastname']+"</p><p class=\"message_time\"><span class=\"glyphicon glyphicon-time\"></span>"+" "+back[key]['message_time']+"</p><p class=\"message_content\">"+back[key]['message']+"</p></td></tr>");
      }
      $('#message_board .panel-body').scrollTop( $('#message_board .panel-body')[0].scrollHeight );
    }
  },
  error:function(){
    //alert("ajax false update the chatboard");
  }
});

//check admin online state
var state_timer=null;
var ajax_state=null;
var flag=0;
state_timer=setInterval(function(){
 ajax_state=$.ajax({
    type:"POST",
    url:"check_admin_state",
    data:{},
    dataType:"json",
    success:function(back)
    {
      var string_online="Hello,I'm online right now.You can send message to me : )";
      var string_outline="Sorry,I'm not online right now.Please feel free to leave message on here or on my message board.Thank you!"
      var string_tip=$("#online_state td .message_content").text();
      if(flag==0)
      {
        $("#online_state .live_time").text(back['message_time']);
        flag=flag+1;
      }
      if(back['state']=="online" && string_tip!=string_online)
      {
        $("#online_state td .message_content").text(string_online);
        $("#online_state .live_time").text(back['message_time']);
        $("#state span").remove();
        $("#state").append("<span style=\"color:green;\" class=\"glyphicon glyphicon-eye-open\"></span>");
        $("#online_music")[0].play();
        //$("#state span").css({color:green});

      }
      if(back['state']=="outline" && string_tip!=string_outline)
      {
        $("#online_state td .message_content").text(string_outline);
        $("#online_state .live_time").text(back['message_time']);
        $("#state span").remove();
        $("#state").append("<span style=\"color:red;\" class=\"glyphicon glyphicon-eye-close\"></span>");
        $("#outline_music")[0].play();
        //$("#state span").css({color:red});
      }
    },
    error:function()
    {
      //alert("ajax false check admin online state");
    }
  });
},500);

//send live message
function chat_input_submit()
{
  var receiver_id=18;
  var message=$("#live_chat .panel-footer input").val();
  if(message!="")
  {
    $("#live_chat .panel-footer input").val("");

    $.ajax({
      type:"POST",
      url:"live_chat_send_message",
      data:{"receiver_id":receiver_id,"message":message},
      dataType:"json",
      async:true,
      success:function(back)
      {
        $("#live_chat .panel-body table").append("<tr><td><p class=\"message_name\"><span class=\"glyphicon glyphicon-home\"></span> "+back['firstname']+" "+back['lastname']+" <span class=\"live_time\">"+back['message_time']+"</span></p><p class=\"message_content\">"+back['message']+"</p></td></tr>");
        $('#live_chat .panel-body').scrollTop( $('#live_chat .panel-body')[0].scrollHeight );
      },
      error:function()
      {
        //alert("ajax false send_live_message");
      }
    });
  }
}


function aircraftbattle()
{
  logout_flag=1;
  clearInterval(receive_timer);
  clearInterval(state_timer);
  window.location.href="aircraftbattle";
}

function snake()
{
  logout_flag=1;
  clearInterval(receive_timer);
  clearInterval(state_timer);
  window.location.href="snake";
}

function chatzone()
{
  logout_flag=1;
  clearInterval(receive_timer);
  clearInterval(state_timer);
  window.location.href="chatzone";
}

function homepage()
{
  logout_flag=1;
  clearInterval(receive_timer);
  clearInterval(state_timer);
  window.location.href="homepage";
}

function game_calculator()
{
  logout_flag=1;
  clearInterval(receive_timer);
  clearInterval(state_timer);
  window.location.href="game_calculator";
}

function logout()
{
  logout_flag=1;
  clearInterval(receive_timer);
  clearInterval(state_timer);
  window.location.href="logout";
}
