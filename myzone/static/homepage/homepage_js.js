//global varibale
	var flag=0;
	//show caption
	function show_caption(obj)
	{
		$(obj).children(".caption").fadeIn();
		$(obj).children(".title").fadeIn();
		$(obj).css("opacity",1);
	}
	//hide caption
	function hide_caption(obj)
	{
		$(obj).children(".caption").fadeOut();
		$(obj).children(".title").fadeOut();
		$(obj).css("opacity",0.6);
	}

	//show subcaption
	function show_subcaption(obj)
	{
		$(obj).children(".subtitle").fadeIn();
	}

	//hide subcaption
	function hide_subcaption(obj)
	{
		$(obj).children(".subtitle").fadeOut();
	}
	//reset element in modal
	function modal_reset()
	{
		$("#pic_collection .modal-body .all_pic").remove();
		$("#pic_collection .modal-header h3").remove();
	}

	//insert description into modal-header
	function insert_description(description)
	{
		$("#pic_collection .modal-header").append("<h3 class=\"modal-title\">"+description+"</h3>");
	}

	//insert picture into modal-body
	function insert_pic(pic_url,description)
	{
		$("#pic_collection .modal-body").append("<div class=\"all_pic\" onmouseenter=\"show_subcaption(this)\"onmouseleave=\"hide_subcaption(this)\" ><img src="+pic_url+"><div class=\"subtitle\"><p>"+description+"</p></div></div></div>");
	}
	//click on graduated day pic show something.
	$("#graduated_day").click(function(){
			//reset element in modal
			modal_reset();
			//description
			insert_description("SFU compus and My graduation ceremony");
			//pictures
			insert_pic("/picture/homepage/sfu_s.png","SFU Surrey Campus");
			insert_pic("/picture/homepage/sfu_b.png","SFU Burnaby Campus");
			insert_pic("/picture/homepage/sfu_d.png","SFU Downtown Campus");
			insert_pic("/picture/homepage/graduated1.png","My friend and me");
			insert_pic("/picture/homepage/graduated.png","My family and me");
			insert_pic("/picture/homepage/graduated3.png","certification");
	});

	//click on wedding day pic show something.
	$("#wedding_day").click(function(){
		//reset element in modal
		modal_reset();
		//description
			insert_description("My excited wedding day");
			//pictures
			insert_pic("/picture/homepage/wedding1.png","Brothers Group");
			insert_pic("/picture/homepage/wedding2.png","Wedding day");
			insert_pic("/picture/homepage/wedding3.png","Handsome Guys");
			insert_pic("/picture/homepage/wedding4.png","Wedding day");
			insert_pic("/picture/homepage/wedding5.png","Wedding day");
			insert_pic("/picture/homepage/wedding6.png","My talent show");
	});

	//click on my hobby pic show something.
	$("#my_hobby").click(function(){
		//reset element in modal
		modal_reset();
		//description
			insert_description("My saxophone");
			//pictures
			insert_pic("/picture/homepage/wedding6.png","My talent show");
			insert_pic("/picture/homepage/sax1.png","Saxophone");
			insert_pic("/picture/homepage/saxphone1.png","certification");
			insert_pic("/picture/homepage/saxphone2.png","certification");
			insert_pic("/picture/homepage/saxphone3.png","certification");
			insert_pic("/picture/homepage/sax6.png","certification");
	});

	//click on vancouver life pic show something.
	$("#vancouver_life").click(function(){
		//reset element in modal
		modal_reset();
		//description
			insert_description("My Vancouver Life");
			insert_pic("/picture/homepage/life1.png","Beautiful river");
			insert_pic("/picture/homepage/life2.png","Rocky mountain");
			insert_pic("/picture/homepage/life3.png","Stanley Park");
			insert_pic("/picture/homepage/life4.png","Top of Vancouver");
			insert_pic("/picture/homepage/life5.png","Whistler");
			insert_pic("/picture/homepage/life6.png","Whistler");
	});

	//dropdown meanu color
	function change_color(obj)
	{
			$(obj).css({background:"#FFFFFF"});
	}
	function reset_color(obj)
	{
			$(obj).css({background:"rgb(236,236,236)"});
	}

	var select_index=0;
	var pre_select_index=0;
	//lunbo caption show and hide
	function lunbo_caption_show(obj)
	{

		var index=$(obj).index();
		index=parseInt(index);
		if(index!=select_index)
		{
			$("#lunbo_container ul li").eq(select_index).children(".lunbo_caption").css("display","none");
			$("#lunbo_container ul li").eq(select_index).stop().animate({width:125},200);
			$(obj).stop().animate({width:300},200);
			//lunbo caption show
			$(obj).children(".lunbo_caption").fadeIn();
		}
		else
		{
			$(obj).children(".lunbo_caption").fadeIn();
		}

	}
	function lunbo_caption_hide(obj)
	{

		var index=$(obj).index();
		index=parseInt(index);

		if(index!=select_index)
		{
			$(obj).stop().animate({width:125},200);
			$("#lunbo_container ul li").eq(select_index).stop().animate({width:300},200);

			//lunbo caption hide
			$(obj).children(".lunbo_caption").fadeOut();
		}

	}
	function select_caption_show()
	{
		$("#lunbo_container ul li").eq(select_index).children(".lunbo_caption").fadeIn();
	}

	function lunbo_select_area(obj)
	{
		var index=$(obj).index();
		index=parseInt(index);
		select_index=index;
		for(var i=0;i<6;i++)
		{
			if(i!=select_index)
			{
				$("#lunbo_container ul li").eq(i).children("img").css({opacity:0.6});
			}
			else
			{
				$("#lunbo_container ul li").eq(i).children("img").css({opacity:1});
			}
		}
		if(pre_select_index!=select_index)
		{
			if(pre_select_index==0)
			{
				setTimeout(function(){
					$("#graduated_day").fadeOut();
				},100);
				setTimeout(function(){
					$("#wedding_day").fadeOut();
				},250);
				setTimeout(function(){
					$("#my_hobby").fadeOut();
				},400);
				setTimeout(function(){
					$("#vancouver_life").fadeOut("normal",fade_out_complete(select_index));
				},550);
		    }
		    else if(pre_select_index==1)
		    {
		    	$("#select_plane").fadeOut("normal",fade_out_complete(select_index));
		    }
		    else if(pre_select_index==2)
			{
				$("#select_snake").fadeOut("normal",fade_out_complete(select_index));
			}
			else if(pre_select_index==3)
			{
				$("#select_calculator").fadeOut("normal",fade_out_complete(select_index));
			}
			else if(pre_select_index==4)
			{
				$("#select_chat").fadeOut("normal",fade_out_complete(select_index));
			}
		    pre_select_index=select_index;
		    //select_area_display(select_index);
		}

	}
	function select_area_display(index)
	{
		if(index==0)
		{
			setTimeout(function(){
				$("#graduated_day").fadeIn();
			},100);
			setTimeout(function(){
				$("#wedding_day").fadeIn();
			},250);
			setTimeout(function(){
				$("#my_hobby").fadeIn();
			},400);
			setTimeout(function(){
				$("#vancouver_life").fadeIn();
			},550);
		}
		else if(index==1)
		{
			$("#select_plane").fadeIn();
		}
		else if(index==2)
		{
			$("#select_snake").fadeIn();
		}
		else if(index==3)
		{
			$("#select_calculator").fadeIn();
		}
		else if(index==4)
		{
			$("#select_chat").fadeIn();
		}
	}
	function fade_out_complete(select_index)
	{
		select_area_display(select_index);
	}

	$("#select_plane").click(function(){
		window.location.href="aircraftbattle";
	});
	$("#select_snake").click(function(){
		window.location.href="snake";
	});
	$("#select_calculator").click(function(){
		window.location.href="game_calculator";
	});
	$("#select_chat").click(function(){
		window.location.href="chatzone";
	});


	$("#log_out").click(function(){
		$.ajax({
			type:"POST",
			url:"logout",
			async:true,
		});
	});
