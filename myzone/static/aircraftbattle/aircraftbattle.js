
var play_state=0;
var all_mousex=0;
var all_mousey=0;

var create_bullet_time=null;
var create_enemy_time=null;

var enemy_array=[];
var bullet_array=[];
var e_number=0;
var b_number=0;

$("#pause").hide();


$("#start").click(function(){

$("#p_music1")[0].pause();
$("#p_music1")[0].currentTime=0;
$("#p_music")[0].play();
$("#pause").show();
$("#start").hide();


$("#c1").hide();
$("#c1").css("display","none");
$("#c1 span1").text(0);
play_state=1;
//mousemove to get x，y position
$(window).mousemove(function(event) {
    if(play_state==0 || play_state==3)
    {
      return;
    }
    var mouse_x = event.originalEvent.x || event.originalEvent.layerX || 0;
    var mouse_y = event.originalEvent.y || event.originalEvent.layerY || 0;

    var plane_left_position=plane_left(mouse_x);
    var plane_top_position=plane_top(mouse_y);

    if(plane_left_position>=0 && (plane_left_position)<=634 && plane_top_position>=14 && (plane_top_position)<=570)
    {
      $("#myplane").css({left:plane_left_position,top:plane_top_position});
      all_mousex=mouse_x;
      all_mousey=mouse_y;
    }
});

//bullet shot
bulletshot();

//create enemy+enemy move
enemy_create();

//after pause recover all bullets move
if(bullet_array.length>0)
{
  restart(1);
}

//after pause recover all enemy move
if(enemy_array.length>0)
{
  restart(2);
}
});

$("#pause").click(function(){
  $("#pause").hide();
  $("#start").show();
  play_state=0;
});

function plane_left(mouse_x)
{
    //get width of panel(include padding,border,and margin).
    var outerwid=$(".panel").outerWidth(true);
    //get width of panel(include padding,border).
    var wid=$(".panel").outerWidth() ;
    //get margin of panel
    var marginleft=(outerwid-wid)/2;
    //get left position of plane
    var plane_left_position=mouse_x-marginleft-33;
    return plane_left_position;
}

function plane_top(mouse_y)
{
    //get height of panel(include padding,border,and margin).
    var outerhei=$(".panel").outerHeight(true);
    //get height of panel(include padding,border).
    var hei=$(".panel").outerHeight() ;
    //get margin of panel
    var margintop=(outerhei-hei)/2;
      //get top position of plane
    var plane_top_position=mouse_y-(margintop+38.6)-40;
    return plane_top_position;
}

//create bullet and initial its position
function bulletshot()
{
  if(create_bullet_time == null)
  {
      create_bullet_time=setInterval(function(){
        if(play_state==0)
        {
          clearInterval(create_bullet_time);
          create_bullet_time=null;
          return;
        }
        var x_position=plane_left(all_mousex);
        var y_position=plane_top(all_mousey);

        var bullet=new Image();
        var bullet_L=x_position+33-3;
        var bullet_T=y_position-14;

        $(bullet).attr("src","/static/aircraftbattle/bullet1.png");
        $(bullet).attr("b_name",b_number);
        b_number=b_number+1;
        $(bullet).addClass("b");
        $(bullet).css({left:bullet_L,top:bullet_T});
        $("#gun")[0].play();

        bullet.time=null;
        bullet_array.push(bullet);

        $("#bullet").append(bullet);

        //bullet move
        bulletmove(bullet);
      },200);
  }

}
//bullet move function
function bulletmove(bullet)
{
  //bullet move 3px on top everytime.
  var speed = -3;
  //10ms move once.
  bullet.time=setInterval(function(){
    var bullettop=bullet.style.top;
    bullettop=parseFloat(bullettop);

    if(play_state==0)
    {
      clearInterval(bullet.time);
      bullet.time=null;

      return;
    }
    if(bullettop<=0)
    {
      clearInterval(bullet.time);
      bullet.time=null;

      bullet_remove(bullet);

      $(bullet).remove();
    }
    else
    {
      bullettop=bullettop+speed;
      $(bullet).css({top:bullettop});
    }
  },10);
}

var enemyObj={
  enemy1:{
    width: 34,
    height: 24,
    score: 100,
    hp: 100,
    bz:"/static/aircraftbattle/bz1.gif"
  },
  enemy2:{
    width: 46,
    height: 60,
    score: 500,
    hp: 500,
    bz:"/static/aircraftbattle/bz2.gif"
  },
  enemy3:{
    width: 110,
    height: 165,
    score: 1000,
    hp: 1000,
    bz:"/static/aircraftbattle/bz3.gif"
  }
}
//create enemy
function enemy_create()
{
  if(create_enemy_time ==null)
  {
      create_enemy_time=setInterval(function(){
      if(play_state==0)
      {
        clearInterval(create_enemy_time);
        create_enemy_time=null;
        return;
      }
      //random initial enemy left position.
      var enemy_L=Math.ceil(Math.random()*$(".panel").width());
      if(enemy_L>20 && enemy_L<587)
      {
        //random the type of enemy.
        var percent_array=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3];
        var enemy_type=percent_array[Math.floor(Math.random()*percent_array.length)];

        var enemyData=enemyObj["enemy"+enemy_type];
        var enemy=new Image();
        var urk="/static/aircraftbattle/enemy"+enemy_type+".png";
        $(enemy).attr("e_name",e_number);
        e_number=e_number+1;
        $(enemy).attr("src",urk);
        $(enemy).attr("planetype",enemy_type);
        $(enemy).attr("plane_width",enemyData.width);
        $(enemy).attr("plane_height",enemyData.height);
        $(enemy).attr("score",enemyData.score);
        enemy.hp=enemyData.hp;
        enemy.bzl=enemyData.bz;
        enemy.score=enemyData.score;
        enemy.time=null;
        $(enemy).css({width:enemyData.width,height:enemyData.height});
        $(enemy).addClass("c");


        //initial enemy top position.
        var enemy_T=-enemyData.height;
        //initial enemy position.
        $(enemy).css({left:enemy_L,top:enemy_T});
        enemy_array.push(enemy);
        $("#enemy").append(enemy);
        //enemy move
        move(enemy);
      }

    },500);
  }


}
//enemy move
function move(enemy)
{
  var speed=0;
  var enemy_type=$(enemy).attr("planetype");
  if(enemy_type==1)
  {
    speed=2;
  }
  else if(enemy_type==2)
  {
    speed=1;
  }
  else
  {
    speed=0.5;
  }
  enemy.time=setInterval(function(){
    var flag=0;
    var flag_1=0;
    //check enemy_collision_bullet
    flag=enemy_collision_bullet(enemy);

    //check enemy_collision_myplane
    flag_1=enemy_collision_myplane(enemy);

    if(flag==1)
    {
      clearInterval(enemy.time);
      enemy.time=null;
      return;
    }
    if(flag_1==1)
    {
      setTimeout(function(){
        play_state==0;
        $("#p_music")[0].pause();
        $("#p_music")[0].currentTime=0;
        $("#p_music1")[0].play();
        $("#c1").css("display","block");
        $("#c1 span1").text($("#getscore span2").text());
        $("#getscore span2").text(0);
        $("#getplane span2").text(0);
        $("#getplane span3").text(0);
        $("#getplane span4").text(0);
        $("#getplane span5").text(0);

        $("#myplane").append("<img src=\"/static/aircraftbattle/my.gif\">");
        $("#myplane").css({left:301,top:570});
        },2000);

      $("#pause").hide();
      $("#start").show();
      return;
    }
    var enemy_T=$(enemy).css("top");
    enemy_T=parseFloat(enemy_T);
    if(play_state==0)
    {
      clearInterval(enemy.time);
      enemy.time=null;
      return;
    }
    if(enemy_T>=650)
    {
      clearInterval(enemy.time);
      enemy.time=null;
      enemy_remove(enemy);
      $(enemy).remove();
    }
    else
    {
      enemy_T=enemy_T+speed;
      $(enemy).css({top:enemy_T});
    }
  },10);
}

//restart game
function restart(type)
{
  if(type==1)
  {
    for(var i=0;i<bullet_array.length;i++)
    {
      bulletmove(bullet_array[i]);
    }
  }
  else
  {
    for(var i=0;i<enemy_array.length;i++)
    {
      move(enemy_array[i]);
    }
  }

}

//remove the select enemy from array
function enemy_remove(enemy)
{
  var name=$(enemy).attr("e_name");
  for(var i=0;i<enemy_array.length;i++)
  {
    if(name == $(enemy_array[i]).attr("e_name"))
    {
      enemy_array.splice(i,1);
      return;
    }
  }
}

//remove the select bullet from array
function bullet_remove(bullet)
{
  var name=$(bullet).attr("b_name");
  for(var i=0;i<bullet_array.length;i++)
  {
    if(name == $(bullet_array[i]).attr("b_name"))
    {
      bullet_array.splice(i,1);
      return;
    }
  }
}

//enemy_collision_bullet
function enemy_collision_bullet(enemy)
{
  var enemy_top=parseFloat($(enemy).css("top"));
  var enemy_left=parseFloat($(enemy).css("left"));


  var enemy_wd=parseInt($(enemy).attr("plane_width"));
  var enemy_ht=parseInt($(enemy).attr("plane_height"));

  for(var i=0;i<bullet_array.length;i++)
  {
    var bullet_top=parseFloat($(bullet_array[i]).css("top"));
    var bullet_left=parseFloat($(bullet_array[i]).css("left"));
    bullet_left=bullet_left+3;


    if(bullet_left>=enemy_left && bullet_left<=(enemy_left+enemy_wd) &&
      bullet_top<=(enemy_top+enemy_ht) && bullet_top<=(enemy_top+enemy_ht*(9/10)))
    {
      enemy.hp=enemy.hp-100;
      if(enemy.hp<=0)
      {
        $("#boom2")[0].play();
        $(bullet_array[i]).remove();
        $(enemy).attr("src",enemy.bzl);

        bullet_remove(bullet_array[i]);
        enemy_remove(enemy);

        setTimeout(function(){
          $(enemy).remove();
        },1000);

        var score=parseInt($("#getscore span2").text());
        score=score+enemy.score;
        $("#getscore span2").text(score);

        var count=parseInt($("#getplane span2").text());
        count=count+1;
        $("#getplane span2").text(count);

        if(enemy.score==1000)
        {
          var count_big=parseInt($("#getplane span3").text());
          count_big=count_big+1;
          $("#getplane span3").text(count_big);
        }
        if(enemy.score==500)
        {
          var count_mid=parseInt($("#getplane span4").text());
          count_mid=count_mid+1;
          $("#getplane span4").text(count_mid);
        }
        if(enemy.score==100)
        {
          var count_small=parseInt($("#getplane span5").text());
          count_small=count_small+1;
          $("#getplane span5").text(count_small);
        }

        return 1;
      }
      else
      {
        $(bullet_array[i]).remove();
        bullet_remove(bullet_array[i]);
      }
    }
  }
}

//enemy_collision_myplane
function enemy_collision_myplane(enemy)
{
  //the position of enemy top and left. The width and height of enemy.
  var enemy_top=parseFloat($(enemy).css("top"));
  var enemy_left=parseFloat($(enemy).css("left"));
  var enemy_wd=parseInt($(enemy).attr("plane_width"));
  var enemy_ht=parseInt($(enemy).attr("plane_height"));

  //The position of my plane.
  var my_left=plane_left(all_mousex);
  var my_top=plane_top(all_mousey);
  var my_wd=66;
  var my_ht=80;

  //condition               left smaller than right                  right larger than left                  top smaller than bottom, bottom larger than top,
  var condition=my_left<=(enemy_left+enemy_wd) && (my_left+my_wd)>=enemy_left && my_top<=(enemy_top+enemy_ht) && (my_top+my_ht)>=enemy_top;

  if(condition)
  {
    $(enemy).attr("src",enemy.bzl);
    $("#myplane img").attr("src","/static/aircraftbattle/bz.gif");

    gameover(enemy);

    reset_array();

    //let mousemove break
    play_state=3;
    setTimeout(function(){
      $(enemy).remove();
    },1000);

    setTimeout(function(){
      $("#myplane img").remove();
    },1500);

    $("#boom")[0].play();
    return 1;

  }

}
function gameover(enemy)
{
    //stop create bullet
    clearInterval(create_bullet_time);
    create_bullet_time=null;
    //stop create enemy
    clearInterval(create_enemy_time);
    create_enemy_time=null;
    var i=0;

    //clean all bullet on the screen
    for(i=0;i<bullet_array.length;i++)
    {
        $(bullet_array[i]).remove();
    }
    //clean all enemy on the screen
    for(i=0;i<enemy_array.length;i++)
    {
      if($(enemy_array[i]).attr("e_name")!=$(enemy).attr("e_name"))
      {
        $(enemy_array[i]).remove();
      }

    }
}

function reset_array()
{
    var i=0;
    for(i=bullet_array.length-1;i>=0;i--)
    {
      clearInterval(bullet_array[i].time);
      bullet_array[i].time=null;
      bullet_array.splice(i,1);
    }
    //clean all enemy on the screen
    for(i=enemy_array.length-1;i>=0;i--)
    {
      clearInterval(enemy_array[i].time);
      enemy_array[i].time=null;
      enemy_array.splice(i,1);
    }
}
