create table users
(
    id int(11) not null auto_increment,
    firstname varchar(128) not null,
    lastname varchar(128) not null,
    phonenumber varchar(128),
    email varchar(128) not null,
    password varchar(256) not null,
    oneline_state int(11) not null default 0,
    primary key(id)
    
)