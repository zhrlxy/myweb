create table messageboard
(
   message_id int(11) not null auto_increment,
   user_id int(11),
   message varchar(128) not null,
   message_time varchar(128) not null,
   primary key(message_id),
   foreign key (user_id) references users(id) on delete cascade
)
