create table chatmessage
(
    note_id int(11) not null auto_increment,
    sender_id int(11),
    receiver_id int(11),
    note text,
    receiver_flag int(11) default 0,
    message_time varchar(128),
    primary key(note_id),
    foreign key(sender_id) references users(id) on delete cascade,
    foreign key(receiver_id) references users(id) on delete cascade
)
