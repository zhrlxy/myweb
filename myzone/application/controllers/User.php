<?php
class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->helper('url');
		$this->load->model('user_modal');
		$this->load->library("session");
	}

	/*login*/
	public function login()
	{
			$this->load->view("user/login");
	}

	public function verify()
	{
		$username=$this->input->post("username");
		$password=md5($this->input->post("password"));
		$result=$this->user_modal->verify_user($username,$password);

		if($result==true)
		{
			$result_id=$this->user_modal->get_userid($username);
			$session_array=array(
				"id"=>$result_id['id'],
				"email"=>$username
			);
			$this->session->set_userdata("logged_in",$session_array);
			if($result_id['id']==18)
			{
				$this->user_modal->change_admin_state_online();
			}
			echo json_encode("success");
		}
		else
		{
			echo json_encode("reject");
		}

	}

	public function logout()
	{
		$result=$this->session->userdata("logged_in");
		$user_id=$result['id'];
		if($user_id==18)
		{
			$this->user_modal->change_admin_state_outline();
		}
		else
		{
			$this->user_modal->change_user_outline_state($user_id);
		}
		$this->session->unset_userdata("logged_in");
		session_destroy();
		redirect(base_url(""));
	}

	public function signin()
	{
		$email=$this->input->post("email");
		$password=md5($this->input->post("password"));
		$lastname=$this->input->post("lastname");
		$firstname=$this->input->post("firstname");
		$phonenumber=$this->input->post("phonenumber");
		$result=$this->user_modal->create_user($email,$password,$lastname,$firstname,$phonenumber);
		echo json_encode($result);
	}

	/*homepage*/
	public function homepage()
	{
		if($this->session->userdata("logged_in"))
		{
			$session_data=$this->session->userdata("logged_in");
			if($session_data['id']==18)
			{
				$data['id']=$session_data['id'];
				$this->load->view("user/chatzone_admin",$data);
			}
			else
			{
				$this->user_modal->change_user_outline_state($session_data['id']);
				$data=$this->user_modal->get_user_name($session_data['id']);
				$this->load->view("user/homepage",$data);
			}
		}
		else
		{
			redirect(base_url(""),"refresh");
		}

	}


	/*chat_zone*/
	public function chatzone()
	{
		if($this->session->userdata("logged_in"))
		{
			$result=$this->session->userdata("logged_in");
			if($result['id']==18)
			{
				$this->load->view("user/chatzone_admin");
			}
			else
			{
				$session_data=$this->session->userdata("logged_in");
				$data['name']=$this->user_modal->get_user_name($session_data['id']);
				$data['id']=$session_data['id'];
				$this->user_modal->change_user_online_state($session_data['id']);
				$this->load->view("user/chatzone",$data);
			}
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}

	public function send_messageboard()
	{
		if($this->session->userdata("logged_in"))
		{
			date_default_timezone_set("America/Vancouver");
			$message_time=date('Y/m/d H:i:s');
			$message=htmlspecialchars($this->input->post("message"));
			$session_data=$this->session->userdata('logged_in');
			$user_id=$session_data['id'];
			$user_id=intval($user_id);

			$data=$this->user_modal->insert_messageboard($message,$user_id,$message_time);

			$firstname=$data['firstname'];
			$lastname=$data['lastname'];
			//echo json_encode($firstname);
			$message_array=array(
					"message"=>$message,
					"date"=>$message_time,
					"firstname"=>$firstname,
					"lastname"=>$lastname
			);
			echo json_encode($message_array);
		}
		else
		{
				redirect(base_url(""),"refresh");
		}

	}

	public function show_messageboard()
	{
		if($this->session->userdata("logged_in"))
		{
			$result=$this->user_modal->get_messageboard();
			echo json_encode($result);
		}
		else
		{
				redirect(base_url(""),"refresh");
		}
	}


	public function check_admin_state()
	{
		if($this->session->userdata("logged_in"))
		{
			date_default_timezone_set("America/Vancouver");
			$message_time=date('Y/m/d H:i:s');

			$result=$this->user_modal->admin_state();
			if($result['state']=="0")
			{
				$array=array(
					"message_time"=>$message_time,
					"state"=>"outline"
				);
				echo json_encode($array);
			}
			if($result['state']=="1")
			{
				$array=array(
					"message_time"=>$message_time,
					"state"=>"online"
				);
				echo json_encode($array);
			}
		}
		else
		{
				redirect(base_url(""),"refresh");
		}
	}


	public function admin_show_all_chat_members()
	{
		if($this->session->userdata("logged_in"))
		{
			$result=$this->user_modal->all_members();
			echo json_encode($result);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}


	//live chat send message
	public function live_chat_send_message()
	{
		if($this->session->userdata("logged_in"))
		{
			date_default_timezone_set("America/Vancouver");
			$message_time=date('Y/m/d H:i:s');

			$result=$this->session->userdata("logged_in");
			$sender_id=$result['id'];

			$receiver_id=$this->input->post("receiver_id");
			$message=htmlspecialchars($this->input->post("message"));

			$data=$this->user_modal->chat_send_message($sender_id,$receiver_id,$message,$message_time);

			$array=array(
				"firstname"=>$data['firstname'],
				"lastname"=>$data['lastname'],
				"message_time"=>$message_time,
				"message"=>$message
			);
			echo json_encode($array);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}


	//live chat receive message
	public function live_chat_receive_message()
	{
		if($this->session->userdata("logged_in"))
		{
			$sender_id=$this->input->post("sender_id");

			$session_data=$this->session->userdata("logged_in");
			$receiver_id=$session_data['id'];

			$result=$this->user_modal->chat_receive_message($sender_id,$receiver_id);

			echo json_encode($result);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}

	}

	//check whether user send message to me.
	public function admin_check_user_message_tome()
	{
		if($this->session->userdata("logged_in"))
		{
			$result=$this->user_modal->check_user_message_tome();
			echo json_encode($result);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}


	/*aircraftbattle*/
	public function aircraftbattle()
	{
		if($this->session->userdata("logged_in"))
		{
			$session_data=$this->session->userdata("logged_in");

			$this->user_modal->change_user_outline_state($session_data['id']);
			$session_data=$this->session->userdata("logged_in");
			$data=$this->user_modal->get_user_name($session_data['id']);
			$this->load->view("user/aircraftbattle",$data);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}

	/*snake*/
	public function snake()
	{
		if($this->session->userdata("logged_in"))
		{
			$session_data=$this->session->userdata("logged_in");

			$this->user_modal->change_user_outline_state($session_data['id']);
			$session_data=$this->session->userdata("logged_in");
			$data=$this->user_modal->get_user_name($session_data['id']);
			$this->load->view("user/snake",$data);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}

	/*game calculator*/
	public function game_calculator()
	{
		if($this->session->userdata("logged_in"))
		{
			$session_data=$this->session->userdata("logged_in");

			$this->user_modal->change_user_outline_state($session_data['id']);
			$session_data=$this->session->userdata("logged_in");
			$data=$this->user_modal->get_user_name($session_data['id']);
			$this->load->view("user/game_calculator",$data);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}


 //check whether user online
	public function check_user_online()
	{
		if($this->session->userdata("logged_in"))
		{
			$result=$this->user_modal->get_user_state();
			echo json_encode($result);
		}
		else
		{
			redirect(base_url(""),"refresh");
		}
	}

}
?>
