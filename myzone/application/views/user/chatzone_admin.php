<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chat Zone</title>
	<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/wangyi_reset.css">
	<link rel="stylesheet" href="/static/chatzone/chatzone_admin.css">
</head>
<style>
</style>
<body>
	<audio id="user_new_music" src="/static/chatzone/user_new_message.mp3"></audio>
	<div id="nav">
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<a class="navbar-brand">James Zone</a>
					<button  class="navbar-toggle"  data-toggle="collapse" data-target="#my_navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div id="my_navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" >
						<li>
						<a class="glyphicon glyphicon-user"> Welcome!Administrator</a>
						</li>
						<li id="log_out">
						<a class="glyphicon glyphicon-log-out" onclick="logout()"> Logout</a>
						</li>
					</ul>
				</div>
			</div>
	</div>
	<div id="message_board">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>
				Message Board (leave something on my website?)
				<span class="glyphicon glyphicon-heart"></span>
				</h3>
			</div>
			<div class="panel-body">
				<table class="table  table-striped table-hover">
				</table>
			</div>
			<div class="panel-footer">
			   <div class="input-group">
			   		<span class="input-group-addon">
			   			<span class="glyphicon glyphicon-pencil"></span>
			   		</span>
			   		<input class="form-control" type="text" onchange="message_input_submit()">
			   		<span class="input-group-btn">
			   			<button class="btn btn-default"><p class="message_submit">Submit</p></button>
			   		</span>

			   </div>

			</div>
		</div>
	</div>
	<div id="live_chat">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Live Chat Members
				      <span class="glyphicon glyphicon-user"></span>
				</h3>
			</div>
			<div class="panel-body">
				<table class="table  table-striped table-hover">
				</table>
			</div>
		</div>
	</div>

  <div id="live_chat_window">
    <div class="panel panel-default">
			<div class="panel-heading">
				<h3>Live Chat</h3>
        <span class="close">&times;</span>
			</div>
			<div class="panel-body">
				<table>
				</table>
			</div>
			<div class="panel-footer">
				<div class="input-group">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-pencil"></span>
					</div>
						<input class="form-control" type="text" onchange="chat_input_submit()">
					<div class="input-group-btn">
						<button class="btn btn-default"><p class="message_submit">Submit</p></button>
					</div>
				</div>
			</div>
		</div>
  </div>

	<span id="id_get"><?php echo $id?></span>

	<script src="/static/bootstrap/js/jquery.min.js"></script>
	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
	<script src="/static/chatzone/chatzone_admin.js"></script>
	<script type="text/javascript">
	</script>

</body>
</html>
