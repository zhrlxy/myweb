<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chat Zone</title>
	<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/wangyi_reset.css">
	<link rel="stylesheet" href="/static/chatzone/chatzone.css">
</head>
<style>
</style>
<body>
	<audio id="online_music" src="/static/chatzone/online.mp3"></audio>
	<audio id="user_new_music" src="/static/chatzone/user_new_message.mp3"></audio>
	<audio id="outline_music" src="/static/chatzone/outline.mp3"></audio>
	<div id="nav">
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<a class="navbar-brand">James Zone</a>
					<button  class="navbar-toggle"  data-toggle="collapse" data-target="#my_navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div id="my_navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" >
						<li class="dropdown">
							<a data-toggle="dropdown" class="glyphicon glyphicon-th-list" href=""> Category<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
									<li>
										<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-plane" onclick="aircraftbattle()" > AircraftBattle</a>
									</li>
									<li>
										<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-knight" onclick="snake()"> GluttonousSnake</a>
									</li>
									<li>
										<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-pencil" onclick="game_calculator()"> GameCalculator</a>
									</li>
									<li>
										<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-envelope" onclick="chatzone()"> ChatZone</a>
									</li>
								</ul>
						</li>
						<li>
						<a class="glyphicon glyphicon-home" onclick="homepage()"> HomePage</a>
						</li>
						<li>
						<a class="glyphicon glyphicon-user"> Welcome! <?php echo $name['firstname']; ?></a>
						</li>
						<li id="log_out">
						<a class="glyphicon glyphicon-log-out" onclick="logout()"> Logout</a>
						</li>
					</ul>
				</div>
			</div>
	</div>
	<div id="message_board">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>
				Message Board (leave something on my website?)
				<span class="glyphicon glyphicon-heart"></span>
				</h3>
			</div>
			<div class="panel-body">
				<table class="table  table-striped table-hover">
				</table>
			</div>
			<div class="panel-footer">
			   <div class="input-group">
			   		<span class="input-group-addon">
			   			<span class="glyphicon glyphicon-pencil"></span>
			   		</span>
			   		<input class="form-control" type="text" onchange="message_input_submit()">
			   		<span class="input-group-btn">
			   			<button class="btn btn-default"><p class="message_submit">Submit</p></button>
			   		</span>
			   </div>

			</div>
		</div>
	</div>
	<div id="live_chat">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Live Chat
				<span class="glyphicon glyphicon-glass"></span>
				<div id="state">
					<span style="color:red;" class="glyphicon glyphicon-eye-close"></span>
				</div>
				</h3>
			</div>
			<div class="panel-body">
				<table>
					<tr id="online_state">
						<td>
							<p class="message_name"><span class="glyphicon glyphicon-home"></span> huanrong Zhang <span class="live_time"></span>
							</p>
							<p class="message_content">Sorry,I'm not online right now.Please feel free to leave message on here or on my message board.Thank you!</p>
						</td>
					</tr>
				</table>
			</div>
			<div class="panel-footer">
				<div class="input-group">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-pencil"></span>
					</div>
						<input class="form-control" type="text" onchange="chat_input_submit()">
					<div class="input-group-btn">
						<button class="btn btn-default"><p class="message_submit">Submit</p></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="live_chat_tip">
		<div class="panel panel-default">
			<div class="panel-heading">
				<p class="title">Do you want to live chat with me online?</p>
			</div>
			<div class="panel-body">
				<p class="content-tip">Tips:</p>
				<p class="content">
				<span class="glyphicon glyphicon-asterisk"></span>
				if I'm online. The eye-icon will open. Such like：
				<span style="color:green;" class="glyphicon glyphicon-eye-open"></span></p>
				<p class="content">
				<span class="glyphicon glyphicon-asterisk"></span>
				if I'm not online. The eye-icon will close. Such like:
				<span style="color:red;" class="glyphicon glyphicon-eye-close"></span>
				</p>
			</div>
			<div class="panel-footer">
				<button class="btn btn-default">Start Live Chat</button>
			</div>
		</div>
	</div>

	<span id="id_get"><?php echo $id?></span>
	<script src="/static/bootstrap/js/jquery.min.js"></script>
	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
	<script src="/static/chatzone/chatzone.js"></script>

</body>
</html>
