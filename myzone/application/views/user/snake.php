<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Snake</title>
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" href="/static/wangyi_reset.css">
    <link rel="stylesheet" href="/static/snake/snake.css">
</head>
<body>
  <div id="nav">
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <a href="homepage" class="navbar-brand">James Zone</a>
          <button  class="navbar-toggle"  data-toggle="collapse" data-target="#my_navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="my_navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right" >
            <li class="dropdown">
              <a data-toggle="dropdown" class="glyphicon glyphicon-th-list" href=""> Category<span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-plane" href="aircraftbattle"> AircraftBattle</a>
                  </li>
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-knight" href="snake"> GluttonousSnake</a>
                  </li>
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-pencil" href="game_calculator"> GameCalculator</a>
                  </li>
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-envelope" href="chatzone"> ChatZone</a>
                  </li>
                </ul>
            </li>
            <li>
            <a class="glyphicon glyphicon-home" href="homepage"> HomePage</a>
            </li>
            <li>
            <a class="glyphicon glyphicon-user"> Welcome! <?php echo $firstname; ?></a>
            </li>
            <li id="log_out">
            <a class="glyphicon glyphicon-log-out" href="logout"> Logout</a>
            </li>
          </ul>
        </div>
      </div>
  </div>
	<div class="container">
		<div class="panel panel-default">
		    	<div class="panel-heading">
		    		<div>
		    			<img style="float:left" class="snake_pic" src="/picture/snake/snake.jpg" alt="">
		    			<img style="float:right" class="snake_pic" src="/picture/snake/snake.jpg" alt="">
              <span>GluttonousSnake</span>
		    		</div>
		    	</div>
		    	<div class="panel-body">
		    	</div>
		    	<div class="panel-footer">
		    		<button id="start" class="btn btn-sm btn-success">Start Game</button>
		    		<button id="pause" class="btn btn-sm btn-warning">Pause</button>
		    		<button id="restart" class="btn btn-sm btn-danger">Restart Game</button>
		    		<button id="game1" class="btn btn-sm btn-info">Version 1</button>
		    		<button id="game2" class="btn btn-sm btn-info">Version 2</button>

		    		<select name="speed" id="speedcontrol">
		    			<option value="10">Crazy</option>
		    			<option value="50">Fast</option>
		    			<option value="120" selected="selected">Normal</option>
		    			<option value="1000">Slow</option>
		    		</select>
		    	</div>

    	</div>
	</div>
    <script src="/static/bootstrap/js/jquery.min.js"></script>
	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
  <script src="/static/snake/snake.js">

  </script>

</body>
</html>
