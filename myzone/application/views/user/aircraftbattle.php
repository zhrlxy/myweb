<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">
	<title>Aircraftbattle</title>
  <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/wangyi_reset.css">
  <link rel="stylesheet" href="/static/aircraftbattle/aircraftbattle.css">
</head>
<body>
  <div id="nav">
      <div class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
          <a href="homepage" class="navbar-brand">James Zone</a>
          <button  class="navbar-toggle"  data-toggle="collapse" data-target="#my_navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="my_navbar" class="collapse navbar-collapse ">
          <ul class="nav navbar-nav navbar-right" >
            <li class="dropdown">
              <a data-toggle="dropdown" class="glyphicon glyphicon-th-list" href=""> Category<span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-plane" href="aircraftBattle"> AircraftBattle</a>
                  </li>
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-knight" href="snake"> GluttonousSnake</a>
                  </li>
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-pencil" href="game_calculator"> GameCalculator</a>
                  </li>
                  <li>
                    <a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-envelope" href="chatzone"> ChatZone</a>
                  </li>
                </ul>
            </li>
            <li>
            <a class="glyphicon glyphicon-home" href="homepage"> HomePage</a>
            </li>
            <li>
            <a class="glyphicon glyphicon-user"> Welcome! <?php echo $firstname; ?></a>
            </li>
            <li id="log_out">
            <a class="glyphicon glyphicon-log-out" href="logout"> Logout</a>
            </li>
          </ul>
        </div>
      </div>
  </div>
		<div id="c2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Aircraftbattle</h3>
					<img id="hp1" src="/static/aircraftbattle/plane2.png" alt="">
					<img id="hp2" src="/static/aircraftbattle/plane3.png" alt="">
				</div>
				<div class="panel-body">
					<div id="back">
						<div id="myplane">
							<img src="/static/aircraftbattle/my.gif" alt="">
							<audio id="gun" src="/static/aircraftbattle/gun.mp3"></audio>
							<audio id="p_music" src="/static/aircraftbattle/plane_music.mp3"  loop="loop"/>
							<audio id="p_music1" autoplay="autoplay" src="http://111.206.91.7/2Q2WEA2641B8019C2F2BE22CA1C5ED8EFF092B4B85AC_unknown_2EF5BE3F4CE9A1AB4765EF8F5E115DAE10AB8490_1/sc1.111ttt.com/2015/1/06/24/99241828278.mp3"  loop="loop"/>
						</div>
						<div id="enemy">
							<audio id="boom" src="/static/aircraftbattle/boom.mp3"></audio>
							<audio id="boom2" src="/static/aircraftbattle/boom2.mp3"></audio>
						</div>
						<div id="bullet">
						</div>
					</div>
					<div id="c1">
						<div>
							<h5>Game Over</h5>
						</div>
						<div>
							<span>Score：</span>
							<span1>0</span>
						</div>
					</div>
				</div>
				<div class="panel-footer">
							<button id="start" class="btn btn-success">Start Game</button>
							<button id="pause" class="btn btn-warning">Pause Game</button>
						<img id="allen" src="/static/aircraftbattle/allen.png" alt="">
						<img id="allen2" src="/static/aircraftbattle/allen.png" alt="">

						<div id="getscore">
							<span1>Score：</span1>
							<span2>0</span2>
						</div>
						<div id="getplane">
							<span1>Destroyed number:</span1>
							<span2>0</span2>
							<span6>(Large:</span6>
							<span3>0</span3>
							<span6>medium:</span6>
							<span4>0</span4>
							<span6>small:</span6>
							<span5>0</span5>
							<span6>)</span6>
						</div>
				</div>
			</div>
		</div>
    <script src="/static/bootstrap/js/jquery.min.js"></script>
  	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
    <script src="/static/aircraftbattle/aircraftbattle.js"></script>
		<script type="text/javascript">
		//dropdown meanu color
		function change_color(obj)
		{
				$(obj).css({background:"#FFFFFF"});
		}
		function reset_color(obj)
		{
				$(obj).css({background:"rgb(236,236,236)"});
		}
		</script>
</body>
</html>
