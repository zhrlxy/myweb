<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Game Calculator</title>
	<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/wangyi_reset.css">
	<link rel="stylesheet" href="/static/gamecalculator/gamecalculator.css">
	</head>
	<body>
		<div id="nav">
				<div class="navbar navbar-default">
					<div class="navbar-header">
						<a href="homepage" class="navbar-brand">James Zone</a>
						<button  class="navbar-toggle"  data-toggle="collapse" data-target="#my_navbar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="my_navbar" class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right" >
							<li class="dropdown">
								<a data-toggle="dropdown" class="glyphicon glyphicon-th-list" href=""> Category<span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
										<li>
											<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-plane" href="aircraftbattle"> AircraftBattle</a>
										</li>
										<li>
											<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-knight" href="snake"> GluttonousSnake</a>
										</li>
										<li>
											<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-pencil" href="game_calculator"> GameCalculator</a>
										</li>
										<li>
											<a  onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-envelope" href="chatzone"> ChatZone</a>
										</li>
									</ul>
							</li>
							<li>
							<a class="glyphicon glyphicon-home" href="homepage"> HomePage</a>
							</li>
							<li>
							<a class="glyphicon glyphicon-user"> Welcome! <?php echo $firstname;?></a>
							</li>
							<li id="log_out">
							<a class="glyphicon glyphicon-log-out" href="logout"> Logout</a>
							</li>
						</ul>
					</div>
				</div>
		</div>
		<h1>
			My favorite Game Damage Calculator
		</h1>
		<div id="t1">
			<p>Skill select Area</p>
			<table class="table table-striped table-bordered table-hover">
				<tbody>
					<tr>
						<td onclick="skill(this)"><span>影杀卷：</span><img src="/picture/gamecalculator/影杀.gif"></td>
						<td onclick="skill(this)"><span>狂影剑：</span><img src="/picture/gamecalculator/狂影.gif"></td>
						<td onclick="skill(this)"><span>百影：</span><img src="/picture/gamecalculator/百影.gif"></td>
					</tr>
					<tr>
						<td onclick="skill(this)"><span>残影剑：</span><img src="/picture/gamecalculator/残影.gif"></td>
						<td onclick="skill(this)"><span>疾影剑：</span><img src="/picture/gamecalculator/疾影.gif"></td>
						<td onclick="skill(this)"><span>断魂剑：</span><img src="/picture/gamecalculator/断魂.gif"></td>
					</tr>
					<tr>
						<td onclick="skill(this)"><span>饮血剑：</span><img src="/picture/gamecalculator/饮血.gif"></td>
						<td onclick="skill(this)"><span>回环剑：</span><img src="/picture/gamecalculator/回环.gif"></td>
						<td onclick="skill(this)"><span>暗器卷：</span><img src="/picture/gamecalculator/暗器.gif"></td>
					</tr>
				</tbody>
			</table>
		</div>


		<table id="t3" class="table table-striped table-bordered">
					<tbody>
						<tr id="skname">
						</tr>
					</tbody>
		</table>

		<div id="t2">
			<table class="table table-striped table-hover table-bordered">
					<p>Skill Damage Calculate Area(please input some attributes)</p>
					<tbody>
						<tr>
								<td><span>Damage：</span><input class="form-control" id="gongli" type="text" placeholder="Only digital number required"></td>
								<td><span>Bonus damage：</span><input class="form-control" id="fushang" type="text" placeholder="Only digital number required"></td>
								<td><span>Critical bonus damage：</span><input class="form-control" id="zhuxin" type="text" placeholder="Only digital number required"></td>
						</tr>
					</tbody>
			</table>
			<input type="button" class="btn btn-default" value="Calculate" onclick="claculate()">
		</div>
		<div id="t4">
			<table class="table table-striped table-hover table-bordered">
						<p>result: </p>
						<tr>
						</tr>
			</table>
			<button class="btn btn-default">Calculate Another Skill?</button>
		</div>
		<script src="/static/bootstrap/js/jquery.min.js"></script>
		<script src="/static/bootstrap/js/bootstrap.min.js"></script>
		<script src="/static/gamecalculator/gamecalculator.js"></script>
	</body>
</html>
