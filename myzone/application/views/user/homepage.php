<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>HomePage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/wangyi_reset.css">
	<link rel="stylesheet" href="/static/homepage/homepage.css">
</head>
<body>
	<div id="nav">
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<a href="homepage" class="navbar-brand">James Zone</a>
					<button  class="navbar-toggle"  data-toggle="collapse" data-target="#my_navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div id="my_navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" >
						<li class="dropdown">
							<a data-toggle="dropdown" class="glyphicon glyphicon-th-list" href=""> Category<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
									<li>
										<a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-plane" href="aircraftbattle"> AircraftBattle</a>
									</li>
									<li>
										<a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-knight" href="snake"> GluttonousSnake</a>
									</li>
									<li>
										<a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-pencil" href="game_calculator"> GameCalculator</a>
									</li>
									<li>
										<a onmouseenter="change_color(this)"  onmouseleave="reset_color(this)" class="glyphicon glyphicon-envelope" href="chatzone"> ChatZone</a>
									</li>
								</ul>
						</li>
						<li>
						<a class="glyphicon glyphicon-home" href="homepage"> HomePage</a>
						</li>
						<li>
						<a class="glyphicon glyphicon-user"> Welcome! <?php echo $firstname; ?></a>
						</li>
						<li>
						<a class="glyphicon glyphicon-log-out" href="logout"> Logout</a>
						</li>
					</ul>
				</div>
			</div>
	</div>

	<div id="lunbo_item">
		<div id="lunbo_container" onmouseleave="select_caption_show()">
			<ul>
				<li class="pic_1" onmouseenter="lunbo_caption_show(this)" onmouseleave="lunbo_caption_hide(this)" onclick="lunbo_select_area(this)">
					<img  src="/picture/homepage/home_back.png">
					<div class="lunbo_caption">
						<p>HomePage</p>
					</div>
				</li>

				<li onmouseenter="lunbo_caption_show(this)" onmouseleave="lunbo_caption_hide(this)"
				onclick="lunbo_select_area(this)">
					<img src="/picture/homepage/plane_back.png">
					<div class="lunbo_caption">
						<p>AircraftBattle</p>
					</div>
				</li>

				<li onmouseenter="lunbo_caption_show(this)" onmouseleave="lunbo_caption_hide(this)"onclick="lunbo_select_area(this)">
					<img src="/picture/homepage/snake_back.png">
					<div class="lunbo_caption">
						<p>GluttonousSnake</p>
					</div>
				</li>

				<li onmouseenter="lunbo_caption_show(this)" onmouseleave="lunbo_caption_hide(this)"onclick="lunbo_select_area(this)">
					<img src="/picture/homepage/game_back.png">
					<div class="lunbo_caption">
						<p>GameCalculator</p>
					</div>
				</li>

				<li onmouseenter="lunbo_caption_show(this)" onmouseleave="lunbo_caption_hide(this)"onclick="lunbo_select_area(this)">
					<img src="/picture/homepage/chat_back.png">
					<div class="lunbo_caption">
						<p>ChatZone</p>
					</div>
				</li>
			</ul>
		</div>
	</div>

	<div id="intro_background">

		<div id="intro_myself">
			<div id="graduated_day" onmouseenter="show_caption(this)" onmouseleave="hide_caption(this)" >
				<img src="/picture/homepage/sfu.png" alt="" data-toggle="modal" data-target="#mymodal">
				<div class="caption">
				</div>
				<div class="title">
					<p>My University</p>
				</div>
			</div>
			<div id="wedding_day" onmouseenter="show_caption(this)" onmouseleave="hide_caption(this)" >
				<img src="/picture/homepage/wedding.png" alt="" data-toggle="modal" data-target="#mymodal">
				<div class="caption">
				</div>
				<div class="title">
					<p>My Wedding Day</p>
				</div>
			</div>
			<div id="my_hobby" onmouseenter="show_caption(this)" onmouseleave="hide_caption(this)" >
				<img src="/picture/homepage/sax4.png" alt="" data-toggle="modal" data-target="#mymodal">
				<div class="caption">
				</div>
				<div class="title">
					<p>My Hobby</p>
				</div>
			</div>
			<div id="vancouver_life" onmouseenter="show_caption(this)" onmouseleave="hide_caption(this)" >
				<img src="/picture/homepage/vancouver.png" alt="" data-toggle="modal" data-target="#mymodal">
				<div class="caption">
				</div>
				<div class="title">
					<p>My Vancouver Life</p>
				</div>
			</div>
			<div id="pic_collection">
						<div class="modal fade" id="mymodal">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<span class="close" data-dismiss="modal">&times;</span>
										</div>
										<div class="modal-body">
										</div>
									</div>
								</div>
						</div>
			</div>
			<div id="select_plane">
				<button class="btn btn-success">
					Click Button To Start AircraftBattle
				</button>
			</div>
			<div id="select_snake">
				<button class="btn btn-warning">Click Button To Start GluttonousSnake</button>
			</div>
			<div id="select_calculator">
				<button class="btn btn-danger">Click Button To Start GameCalculator</button>
			</div>
			<div id="select_chat">
				<button class="btn btn-info">Click Button To Enter ChatZone</button>
			</div>
		</div>
	</div>
	<script src="/static/bootstrap/js/jquery.min.js"></script>
	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
	<script src="/static/homepage/homepage_js.js"></script>
</body>
</html>
