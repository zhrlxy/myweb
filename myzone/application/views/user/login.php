<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/wangyi_reset.css">
	<link rel="stylesheet" href="/static/login/login.css">
</head>
<body>
	<div id="nav">
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<a href="" class="navbar-brand">James Zone</a>
					<button  class="navbar-toggle"  data-toggle="collapse" data-target="#my_navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div id="my_navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" >
						<li id="login_button">
						<a class="glyphicon glyphicon-home" href="#" data-toggle="modal" data-target="#mymodal"> Login</a>
						</li>
					</ul>
				</div>
			</div>
	</div>
	<div id="intro_web">
			<div id="intro_web_1" onmouseenter="intro_web_opacity_show(this)"  onmouseleave="intro_web_opacity_hide(this)">
				<h1>Welcome to James Zone</h1>
			</div>
			<div id="intro_web_2" onmouseenter="intro_web_opacity_show(this)" onmouseleave="intro_web_opacity_hide(this)">
				<h2>Introducation:</h2>
				<h3>·My Name:Huanrong Zhang</h3>
				<h3>·Graduated School:Simon Fraser University</h3>
				<h3>·My Major:Computer Science</h3>
				<h3>·This Website is designed by myself</h3>
				<p>·Front-end:Html,CSS,Javascript,Jquery,Bootstrap</p>
				<p style="padding-bottom:60px;">·Back-end:CodeIgniter(PHP frame),MySQL,Apache</p>
			</div>
			<div id="intro_web_3" onmouseenter="intro_web_opacity_show(this)" onmouseleave="intro_web_opacity_hide(this)">
				<h2>My Website introducation:</h2>
				<h3>·See photos of my life and school</h3>
				<h3>·Enjoy my game</h3>
				<h3>·Connect with me</h3>
			</div>
	</div>
	<div id="signin_area">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 style="font-weight:800;font-size:20px;text-align:center;">Please Signup</h2>
			</div>
			<div class="panel-body">
				<form>
					<div class="form-group">
					<label id="signin_email_tip" for="">Email:(required)</label>
						<div class="input-group">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-envelope"></span>
							</div>
							<input id="signin_email" class="form-control" type="text" name="email">
						</div>
					</div>
					<div class="form-group">
						<label id="signin_password_tip" for="">Password:(required)</label>
						<div class="input-group">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-lock"></span>
							</div>
							<input  id="signin_password" class="form-control" type="password" name="password">
						</div>
					</div>
					<div class="form-group">
							<label id="signin_lastname_tip" for="">Lastname:(required)</label>
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-user"></span>
								</div>
								<input id="signin_lastname" class="form-control" type="text" name="lastname">
							</div>
					</div>
					<div class="form-group">
						<label id="signin_firstname_tip" for="">Firstname:(required)</label>
						<div class="input-group">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-user"></span>
							</div>
							<input id="signin_firstname" class="form-control" type="text" name="firstname">
						</div>
					</div>
					<div class="form-group">
						<label  id="signin_phonenumber_tip" for="">Phonenumber:(option)</label>
						<div class="input-group">
							<div class="input-group-addon">
								<span  class="glyphicon glyphicon-phone"></span>
							</div>
							<input id="signin_phonenumber" class="form-control" type="text" name="phonenumber">
						</div>
					</div>
					<div id="Signin">Sign up</div>
				</form>
			</div>
		</div>
		<div class="sign_up_success">
			<h2>Congratulation!</h2>
			<h2>Sign up successful!</h2>
			<div id="sign_login" data-toggle="modal" data-target="#mymodal">Login</div>
		</div>
	</div>
	<div class="modal fade" id="mymodal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<span class="close" data-dismiss="modal">&times;</span>
					<h2 style="font-weight:800;font-size:20px;text-align:center;">Please Login</h2>
				</div>
				<div class="modal-body">
					<div>
						<form>
							<div class="form-group">
									<label id="email_tip" for="">Email:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-user"></span>
										</div>
										<input id="username" class="form-control" type="text" name="username" placeholder="Please enter your Email">
									</div>
							</div>
							<div class="form-group">
								<label id="password_tip" for="">Password:</label>
								<div class="input-group">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-lock"></span>
									</div>
									<input  id="password" class="form-control" type="password" name="password" placeholder="Please enter your Password">
								</div>
							</div>
							<div id="submit">Submit<div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
	<script src="/static/bootstrap/js/jquery.min.js"></script>
	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
	<script src="/static/login/login.js"></script>
</body>
</html>
