<?php
class User_modal extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	/*login*/
	public function verify_user($username,$password)
	{
		$sql="select * from users where '$username'=email and '$password'=password";
		$query=$this->db->query($sql);
		$result=$query->row_array();
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_userid($username)
	{
		$sql="select id from users where '$username'=email";
		$query=$this->db->query($sql);
		$result=$query->row_array();
		return $result;
	}

	public function create_user($email,$password,$lastname,$firstname,$phonenumber)
	{
		$sql="select * from users where '$email'=email";
		$query=$this->db->query($sql);
		$result=$query->row_array();
		if($result)
		{
			return false;
		}
		else
		{
				$data=array(
					'firstname'=>$firstname,
					'lastname'=>$lastname,
					'email'=>$email,
					'password'=>$password,
					'phonenumber'=>$phonenumber,
				);
				$this->db->insert('users',$data);
				return true;
		}
	}

	/*chat_zone*/
	public function insert_messageboard($message,$user_id,$message_time)
	{

		$data=array(
			'user_id'=>$user_id,
			'message'=>$message,
			'message_time'=>$message_time,
		);
		$this->db->insert('messageboard',$data);

		$sql="select firstname,lastname from users where $user_id=id";
		$query=$this->db->query($sql);
		return $query->row_array();
	}

	public function get_messageboard()
	{
		 $sql="select B.user_id,A.firstname,A.lastname,B.message,B.message_time from users A, messageboard B where A.id=B.user_id";
		 $query=$this->db->query($sql);
		 return $query->result_array();
	}


	public function admin_state()
	{
		$sql="select state from adminstate where id=1";
		$query=$this->db->query($sql);
		return $query->row_array();
	}

	public function change_admin_state_online()
	{
		$sql="update adminstate set state='1' where id=1";
		$query=$this->db->query($sql);
	}

	public function change_admin_state_outline()
	{
		$sql="update adminstate set state='0' where id=1";
		$query=$this->db->query($sql);
	}

	public function all_members()
	{
		$sql="select id,firstname,lastname from users where id!=18";
		$query=$this->db->query($sql);
		return $query->result_array();
	}


	public function chat_send_message($sender_id,$receiver_id,$message,$message_time)
	{
		$data=array(
			"sender_id"=>$sender_id,
			"receiver_id"=>$receiver_id,
			"note"=>$message,
			"receiver_flag"=>0,
			"message_time"=>$message_time
		);
		$this->db->insert("chatmessage",$data);
		$sql="select firstname,lastname from users where $sender_id=id";
		$query=$this->db->query($sql);
		return $query->row_array();
	}


	public function chat_receive_message($sender_id,$receiver_id)
	{
		$sql="select note,message_time from chatmessage where $sender_id=sender_id and $receiver_id=receiver_id and receiver_flag=0";
		$query=$this->db->query($sql);
		$result=$query->result_array();
		$sql1="update chatmessage set receiver_flag=1 where $sender_id=sender_id and $receiver_id=receiver_id and receiver_flag=0";
		$query1=$this->db->query($sql1);
		return $result;
	}

	public function check_user_message_tome()
	{
		$sql="select sender_id from chatmessage where sender_id!=18 and receiver_id=18 and receiver_flag=0";
		$result=$this->db->query($sql);
		return $result->result_array();
	}


	public function get_user_name($id)
	{
		$sql="select firstname,lastname from users where id=$id";
		$query=$this->db->query($sql);
		return $query->row_array();
	}


	//change user online state
	public function change_user_online_state($id)
	{
		$sql="update users set oneline_state=1 where $id=id";
		$query=$this->db->query($sql);
	}

	public function change_user_outline_state($id)
	{
		$sql="update users set oneline_state=0 where $id=id";
		$query=$this->db->query($sql);
	}

	public function get_user_state()
	{
		$sql="select id,oneline_state from users where id!=18";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
}
?>
